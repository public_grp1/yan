
masters=("SQL-MASTER01" "SQL-MASTER02")
echo "Starting Script..."
#Get IP from the containers
MASTER01_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${masters[0]})
echo "Master 1 IP : $MASTER01_IP "
MASTER02_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${masters[1]})
echo "Master 2 IP : $MASTER02_IP "

check_error() {
    if [ $? -ne 0 ]; then
        echo "Error occurred. Exiting script."
        exit 1
    fi
}

for master in "${masters[@]}"; do
    echo "Creating replication user for $master..."
    docker exec -i "$master" mysql -uroot -prootpassword -e \
    "SET GLOBAL sync_binlog=1;GRANT REPLICATION SLAVE ON *.* TO 'replication_user'@'%' IDENTIFIED BY 'replication_password';FLUSH PRIVILEGES;FLUSH TABLES WITH READ LOCK;SHOW MASTER STATUS;"
    
    check_error
    


    

    echo "Stopping Current Slave..."
    docker exec -it "$master" mysql -u root -prootpassword -e "STOP SLAVE;RESET SLAVE;"

    check_error
    


    echo "server id for $master is $(docker exec -it "$master" mysql -u root -prootpassword -e "SHOW VARIABLES LIKE 'server_id';" )"
    echo "Permission for replication_user on $master are: $(docker exec -it "$master" mysql -u root -prootpassword -e "SHOW GRANTS FOR 'replication_user'@'%';")"
    echo "Changing master..."
    
    CURRENT_MASTER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$master")
    echo "$master IP: $CURRENT_MASTER_IP"

    if [[ "$CURRENT_MASTER_IP" == "$MASTER01_IP" ]]; then

        echo "Getting FILE, POSITION, and CURRENT IP values for ${masters[1]}..."
        FILE=$(docker exec -it ${masters[1]} mysql -u root -prootpassword -e "SHOW MASTER STATUS" | awk 'NR==4{print $2}' )
        echo "FILE: $FILE"
        POSITION=$(docker exec -it ${masters[1]} mysql -u root -prootpassword -e "SHOW MASTER STATUS" | awk 'NR==4{print $4}' )
        echo "POSITION: $POSITION"



        echo "Setting $master as Slave of ${masters[1]}"
        echo "FILE is $FILE and POSITION is $POSITION IP is $MASTER02_IP"
        docker exec -i $master mysql -u root -prootpassword -e \
        "CHANGE MASTER TO MASTER_HOST='${masters[1]}', MASTER_PORT=3306, MASTER_USER='replication_user', MASTER_PASSWORD='replication_password', MASTER_LOG_FILE='$FILE', MASTER_LOG_POS=$POSITION;"

    else

        echo "Getting FILE, POSITION, and CURRENT IP values for ${masters[1]}..."
        FILE=$(docker exec -it ${masters[1]} mysql -u root -prootpassword -e "SHOW MASTER STATUS" | awk 'NR==4{print $2}' )
        echo "FILE: $FILE"
        POSITION=$(docker exec -it ${masters[1]} mysql -u root -prootpassword -e "SHOW MASTER STATUS" | awk 'NR==4{print $4}' )
        echo "POSITION: $POSITION"



        echo "Setting $master as Slave of ${masters[0]}"
        echo "FILE is $FILE and POSITION is $POSITION IP is $MASTER01_IP"
        docker exec -i $master mysql -u root -prootpassword -e \
        "CHANGE MASTER TO MASTER_HOST='${masters[0]}', MASTER_PORT=3306, MASTER_USER='replication_user', MASTER_PASSWORD='replication_password', MASTER_LOG_FILE='$FILE', MASTER_LOG_POS=$POSITION;"
    fi
    


    check_error

    docker exec -i "$master" mysql -u root -prootpassword -e "START SLAVE;"
    


    check_error

    echo "Printing slave status for $master: "
    docker exec -it "$master" mysql -u root -prootpassword -e "SHOW SLAVE STATUS \G"

    check_error
    


done


echo "Script ran his course"