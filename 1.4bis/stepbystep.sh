docker exec -it SQL-MASTER01bis bash
#   On SQL-MASTER01


cat <<EOF >> /etc/mysql/mariadb.conf.d/50-server.cnf
#replication
server_id = 1
report_host = master
log_bin = /var/lib/mysql/mariadb-bin
log_bin_index = /var/lib/mysql/mariadb-bin.index
relay_log = /var/lib/mysql/relay-bin
relay_log_index = /var/lib/mysql/relay-bin.index
EOF

service mariadb restart
docker start SQL-MASTER01bis && docker exec -it SQL-MASTER01bis bash

mysql -e "create user 'test_master'@'%' identified by 'test_master';grant replication slave on *.* to 'test_master'@'%';"

mysql -e "show master status;" 


###################################################
docker exec -it SQL-MASTER02bis bash
#On SQL-MASTER02 
cat <<EOF >> /etc/mysql/mariadb.conf.d/50-server.cnf
#replication
server_id = 2
report_host = master2
log_bin = /var/lib/mysql/mariadb-bin
log_bin_index = /var/lib/mysql/mariadb-bin.index
relay_log = /var/lib/mysql/relay-bin
relay_log_index = /var/lib/mysql/relay-bin.index
EOF

service mariadb restart
docker start SQL-MASTER02bis && docker exec -it SQL-MASTER02bis bash

mysql -e "create user 'test_master2'@'%' identified by 'test_master2';grant replication slave on *.* to 'test_master2'@'%';"
mysql -e "show master status;"
file02=$(mysql -e "SHOW MASTER STATUS" | awk 'NR==2{print $1}')
position02=$(mysql -e "show master status;" | awk 'NR==2{ print $2}')
mysql -e "STOP SLAVE;"
mysql -e  "CHANGE MASTER TO MASTER_HOST='SQL-MASTER01bis', MASTER_USER='test_master', MASTER_PASSWORD='test_master', MASTER_LOG_FILE='$file02', MASTER_LOG_POS=$position02;"
mysql -e "START SLAVE;"
mysql -e "show slave status \G"

# BACK TO MASTER01
mysql -e STOP SLAVE;
file01=$(mysql -e "SHOW MASTER STATUS" | awk 'NR==2{print $1}')
position01=$(mysql -e "show master status;" | awk 'NR==2{ print $2}')
mysql -e "CHANGE MASTER TO MASTER_HOST='SQL-MASTER02bis', MASTER_USER='test_master2', MASTER_PASSWORD='test_master2', MASTER_LOG_FILE='$file01', MASTER_LOG_POS=$position01;"
mysql -e "START SLAVE;"
mysql -e "show slave status \G"